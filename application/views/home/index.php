<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jornada de trabajo</title>
    <link href="http://hangman.local/assets/css/bootstrap.min.css" rel="stylesheet">
    <style>
      body{
        font-family: Comic Sans MS;
        background-image:url('http://cdn1.shopmania.biz/files/s1/197100995/p/l/6/papel-pintado-infantil-douce-nuit-casadeco-rayas-2cm-celeste~645386.jpg'); 
      }
      #contenedor{
         border: 10px solid #5F6A92;
         background-color: #75AF96;
         width:95%;
         padding:5px;
         min-height: 50em;
         line-height:30px;
         background-position: center center;  
         margin-left: 2%; 
      }
    </style>
  </head>
  <body>
    <br>
    <br>
    <br>

    <div id="contenedor">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <br>
          <h1 class="bg-warning">Bienvenido<br> <small>listo para preparar tu jornada de trabajo?...</small> </h1>
          <br>
        </div>
        <div class="col-md-6 col-md-offset-4">
          <img src="http://hangman.local/assets/img/img_home.jpg" class="img-circle">
          <br>
        </div>
        <div class="col-md-4 col-md-offset-4">
          <br>
          <button type="button" class="btn btn-primary btn-lg btn-block">Iniciar jornada!</button>
          <br>
        </div>
      </div>
    </div>
  </body>
</html>